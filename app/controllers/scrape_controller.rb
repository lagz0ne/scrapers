class ScrapeController < ApplicationController

	# Setup the hash of scrapers

	def index
		scraper = params['scraper']

		return render json: {success: false, message: "Couldn't find scraper #{scraper}"} unless BaseScraper.contains?(scraper)
		
		scraper = BaseScraper.get(scraper)

		return render json: {success: false, message: scraper.erros} if !scraper.prepare_ferry_search(params).valid?

		scraper.parse
		return reder json: {success: false, message: scraper.errors} if !scraper.valid?
		
		return render json: {sucess: true, message: scraper.results}
	end
end