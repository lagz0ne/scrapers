Scrapers::Application.routes.draw do

  get '/scrape/:scraper/:from/:to', to: 'scrape#index', constraints: { scraper: /[\w\d\.\-_]*/}
  get '/scrape/:scraper/:from/:to/:outbound_date', to: 'scrape#index', constraints: { scraper: /[\w\d\.\-_]*/}
  get '/scrape/:scraper/:from/:to/:outbound_date/:inbound_date', to: 'scrape#index', constraints: { scraper: /[\w\d\.\-_]*/}

end