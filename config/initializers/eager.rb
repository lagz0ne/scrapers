# Eager loading all of scrapers
# Use this facility to get rid of phantom name for <scraper>.rb
Rails.logger.info 'Loading all scrapers under /extras/scrapers'
Dir["#{Rails.root}/extras/scrapers/*.rb"].each do |file| 
	load file
end