require 'curb'
require 'nokogiri'
require 'date'

class DirectFerries < BaseScraper

	# Unique id per scraper
	set_id 'directferries.co.uk'

	def prepare_search
		# This method will be called by BaseScraper
		# This is the place to write per-scraper logic
		currency = '1' if @param.currency == 'GBP'
		currency = '2' if currency.nil? and @param.currency == 'EUR'
		
		@param.others[:currency] = currency
		@param.others[:trip] = @param.is_roundtrip.to_s.capitalize
		@param.others[:trip_s] = @param.is_roundtrip ? '1' : '2'
		@param.others[:route] = '66' # Fixed liverpool - dublin route. The logic to read stuff from configuration should goes here
	end

	def parse
		# Per instance client
		client = Curl::Easy.new

		# client.verbose = true
		client.enable_cookies = true
		client.useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36'

		url = setup_url
		# Get VIEW_STATE from homepage
		puts '=== Retrieving VIEW_STATE ==='

		client.url = url
		client.perform

		view_state = retrieve_home_page_view_state(client.body_str)
		return if invalid?

		search_param = setup_search_condition
		condition_param = search_param | [
			Curl::PostField.content('__VIEWSTATE', view_state),
			Curl::PostField.content('__EVENTTARGET', 'cal_out'),
			Curl::PostField.content('__EVENTARGUMENT', ''),
			Curl::PostField.content('__LASTFOCUS', ''),
			Curl::PostField.content('__ASYNCPOST', 'true')]

		# Change the outbound date and outbound time
		client.http_post(*condition_param)

		view_state = retrieve_view_state(client.body_str)
		return if invalid?

		puts '=== Posting to result page ==='
		result_page_param = search_param | [
			Curl::PostField.content('butSubmit', 'Get Quote'),
			Curl::PostField.content('__EVENTTARGET', ''),
			Curl::PostField.content('__EVENTARGUMENT', ''),
			Curl::PostField.content('__LASTFOCUS', ''),
			Curl::PostField.content('__VIEWSTATE', view_state)]
		
		# Getting result page
		client.http_post(*result_page_param)

		parse_fares(client.body_str.force_encoding('UTF-8'))
	end

	def setup_url
		"https://ssl.directferries.com/ferry/secure/multi_price_detail.aspx?stdc=DF10&grid=0&rfid=#{@param.others[:route]}&retn=#{@param.others[:trip]}&curr=#{@param.others[:currency]}&rfidr=0&mcdr=0"
	end

	def setup_search_condition
		arr = [
			Curl::PostField.content('ctl09$radJourney', @param.others[:trip_s]), # roundtrip or oneway
			Curl::PostField.content('ctl09$ddRouteFilter', '66'), # The route should be in the config
			Curl::PostField.content('ctl09$ddCurrency', @param.others[:currency]), # Currency of EUR or GBP, should be in config as well
			Curl::PostField.content('ctl09$hfMiniCruise', '0'), # ... no idea
			Curl::PostField.content('cal_out', @param.outbound_date.strftime('%d %B %Y')),
			Curl::PostField.content('ddOutTime', @param.outbound_time),
			Curl::PostField.content('passengerAges$Pets1$ddlPetTypes', '0'),
			Curl::PostField.content('passengerAges$Pets2$ddlPetTypes', '0'),
			Curl::PostField.content('passengerAges$Pets3$ddlPetTypes', '0'),
			Curl::PostField.content('vehicleDetails$HasVehicle', 'radNoVehicle')
		]
		if @param.is_roundtrip
			arr.push Curl::PostField.content('cal_ret', @param.inbound_date.strftime('%d %B %Y'))
			arr.push Curl::PostField.content('ddRetTime', @param.inbound_time)
		end

		arr.push Curl::PostField.content('passengerAges$ddlTotalPassengers', @param.adults_count)
		(1..@param.adults_count).each{|n| arr.push(Curl::PostField.content("passengerAges$Age#{n}$ddlAges", 18))} #Forever 18

		arr
	end

	def retrieve_home_page_view_state(content)
		root = Nokogiri::HTML(content)

		if root.css('input#__VIEWSTATE').empty?
			add_error "The layout may be changed, couldn't retrieve __VIEWSTATE from home page anymore"
		else
			root.css('input#__VIEWSTATE')[0]['value']
		end
	end

	def retrieve_view_state(content)
		if content.index('__VIEWSTATE|').nil?
			add_error "The search setting seems incorrect, couldn't retrieve __VIEWSTATE"
		else 
			start_point = content.index('__VIEWSTATE|') + '__VIEW_STATE|'.length - 1
			content = content[start_point..-1]
			content = content[0..(content.index('|') - 1)]
		end
	end

	def parse_fares(content) 
		root = Nokogiri::HTML(content)
		@results = [] # new result every time

		# Grab anything that has price only
		root.css('div.price2').each do |price_element|
			next if price_element.text.strip.empty?

			fare_element = price_element.parent

			from_element = fare_element.css('div.info1')
			to_element = fare_element.css('div.info2')
			duration_element = fare_element.css('div.duration')

			fare = {
				:price => price_element.css('p.pricetxt').text,
				:outbound => {
					:origin => from_element.css('p.porttxt').text,
					:destination => to_element.css('p.porttxt').text,
					:departure_time => from_element.css('p:eq(2)').children.select(&:text?).first.text,
					:arrival_time => to_element.css('p:eq(2)').children.select(&:text?).first.text,
					:duration => duration_element.css('.dur1').text
				}
			}

			if @param.is_roundtrip
				fare[:inbound] = {
					:origin => from_element.css('p.porttxt2').text,
					:destination => to_element.css('p.porttxt2').text,
					:departure_time => from_element.css('p:eq(5)').children.select(&:text?).first.text,
					:arrival_time => to_element.css('p:eq(5)').children.select(&:text?).first.text,
					:duration => duration_element.css('.dur2').text
				}
			end

			@results.push fare
		end
	end

end