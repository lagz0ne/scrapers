# All of scrapers is being loaded eagerly inside eager.rb
require 'active_model'

class FerryParam
	include ActiveModel::Validations

	attr_accessor :others, :from, :to, :outbound_date, :outbound_date_string, :outbound_time, :inbound_date_string, :inbound_date, :inbound_time, :adults_count, :is_roundtrip, :currency # :with_car ...

	validates_presence_of :from, :to
	validates_format_of :outbound_date_string, :inbound_date_string, :with => /([2-9]\d{3}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|(([2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00))0229)/, :allow_nil => true

	def self.create(params = {})
		instance = self.new(params)

		if instance.valid?
			instance.outbound_date = instance.outbound_date_string.nil? ? Date.today + 3 : Date.strptime(instance.outbound_date_string, '%Y%m%d')
			instance.inbound_date = Date.strptime(instance.inbound_date_string, '%Y%m%d') if instance.is_roundtrip
		end

		instance
	end

	private
	# Transfer from anonymous hash to meaningful param object
	def initialize(params = {})
		@from = params['from'] || params[:from]
		@to = params['to'] || params[:to]

		@outbound_date_string = params['outbound_date']
		@outbound_time = params['outbound_time'] || '8' # Set it to 8am by default

		@inbound_date_string = params['inbound_date']
		@inbound_time = params['inbound_time'] || '20' # Set it to 7pm by default 

		@is_roundtrip = !@inbound_date_string.nil?

		@adults_count = params['adults_count'] || 1
		@currency = params['currency'] || 'EUR'
		# Vehicle should be here, but number of combinations too high, so, let it none at first

		@others = {} # Store per scraper parameter
	end

end

class BaseScraper

	@@subclasses = {}
		
	# Static methods
	# Store id as instance var
	def self.set_id id
		# Store class definition to access later
		@id = id
		@@subclasses[@id] = self
	end

	# Get all available scraper
	def self.army
		@@subclasses
	end

	# Check the availability of scraper based on unique id
	def self.contains? scraper
		@@subclasses.has_key?(scraper)
	end

	# New instance for every new request, threadsafety storing instance object
	def self.get scraper
		@@subclasses[scraper].new
	end

	# Instance methods
	def initialize
		@halted = false
		@message = []
	end

	# Prepare the search object based on the input parameters. 
	def prepare_ferry_search params
		@param = FerryParam.create(params)
		prepare_search if defined? prepare_search
		self
	end

	def add_error message
		@halted = true
		@message.push message
	end

	def errors 
		return @message if @halted
		return @param.errors if !@param.valid?
	end

	# Bring valid method to scraper instance
	def valid?
		# Call per scraper prepare method
		@param.valid? && !@halted
	end

	def invalid?
		!valid?
	end

	def results
		@results
	end

end