require '../scrapers/base_scraper'
require '../scrapers/directferries.co.uk'
require 'test/unit'

class TestDirectFerriesScraper < Test::Unit::TestCase
	def setup
		@oneway_param = {'from' => 'Liverpool', 'to' => 'Dublin', 'outbound_date' => '20131014'}
		@roundtrip_param = {'from' => 'Liverpool', 'to' => 'Dublin', 'outbound_date' => '20131012', 'inbound_date' => '20131014'}

		@oneway_scraper = BaseScraper.get('directferries.co.uk')
		@oneway_scraper.prepare_ferry_search(@oneway_param)

		@roundtrip_scraper = BaseScraper.get('directferries.co.uk')
		@roundtrip_scraper.prepare_ferry_search(@roundtrip_param)
	end

	def test_parse_oneway
		assert(@oneway_scraper.valid?, "Shouldn't contain any errors")
		@oneway_scraper.parse

		assert(@oneway_scraper.valid?, "It shouldn't contain any errors after parsing")
		puts @oneway_scraper.results
	end

	def test_parse_roundtrip
		assert(@roundtrip_scraper.valid?, "Shoudln't contain any errors")
		@roundtrip_scraper.parse

		assert(@roundtrip_scraper.valid?, "Shouldln't contain any errors after parsing")
		puts @roundtrip_scraper.results
	end

	def test_setup_url
		assert(@oneway_scraper.setup_url == 'https://ssl.directferries.com/ferry/secure/multi_price_detail.aspx?stdc=DF10&grid=0&rfid=66&retn=False&curr=2&rfidr=0&mcdr=0', 'The URL must be matched')
		assert(@roundtrip_scraper.setup_url == 'https://ssl.directferries.com/ferry/secure/multi_price_detail.aspx?stdc=DF10&grid=0&rfid=66&retn=True&curr=2&rfidr=0&mcdr=0', 'The URL must be matched')
	end

	def test_retrieve_home_page_view_state
		content = <<-TEXT
			Line 1
			Line 2
			<input type='hidden' name='__VIEWSTATE' value='This is an important param' id='__VIEWSTATE' />
			Someother line after that
		TEXT
		assert(@oneway_scraper.retrieve_home_page_view_state(content) == 'This is an important param')

		wrong_content = <<-TEXT
			Line 1
			Line 2
			<input type='hidden' name='__VIEWSTATE' value='This is an important param' id='__VIEWSTATE__' />
			Someother line after that
		TEXT
		@oneway_scraper.retrieve_home_page_view_state(wrong_content)
		assert(@oneway_scraper.invalid?, "Shouldn't get it passed")
	end

	def test_retrieve_view_state
		content = <<-TEXT
			First line
			Something like this dtcde4fr5vgtgt60xccv4br56tn7m8912|frvgn98nb890123234dfw|__VIEWSTATE|/This text is important|rf76gt8uhy9i|174hy7ur9efsad|
			Next line
		TEXT
		assert(@roundtrip_scraper.retrieve_view_state(content) == '/This text is important')

		wrong_content = <<-TEXT
			First line
			Something like this dtcde4fr5vgtgt60xccv4br56tn7m8912|frvgn98nb890123234dfw|__VIEWSTATE__|/This text is important|rf76gt8uhy9i|174hy7ur9efsad|
			Next line
		TEXT
		assert(@roundtrip_scraper.retrieve_view_state(wrong_content) != '/This text is important')
	end

end