require '../scrapers/base_scraper'
require 'test/unit'

class TestBaseScraper < Test::Unit::TestCase

	def setup
		@missing_from_param = {'to' => 'Dublin', 'outbound_date' => '201301012'}
		@missing_to_param = {'from' => 'Liverpool', 'outbound_date' => '201301012'}
		@invalid_date_format_outbound = {'from' => 'Liverpool', 'to' => 'Dublin', 'outbound_date' => 'incorrect pattern'}
		@invalid_date_format_inbound = {'from' => 'Liverpool', 'to' => 'Dublin', 'outbound_date' => '20131012', 'inbound_date' => 'incorrect pattern'}
		
		@correct_param_1 = {'from' => 'Liverpool', 'to' => 'Dublin', 'outbound_date' => '20131012', 'inbound_date' => '20131014'}
		@correct_param_2 = {'from' => 'Liverpool', 'to' => 'Dublin', 'outbound_date' => '20131012'}
		@correct_param_3 = {'from' => 'Liverpool', 'to' => 'Dublin'}
	end

	def test_ferry_param_class
		# Check validation
		missing_from_param = FerryParam.create(@missing_from_param)
		assert(missing_from_param.invalid? && !missing_from_param.errors[:from].empty?, 'Param should be invalid without :from')

		missing_to_param = FerryParam.create(@missing_to_param)
		assert(missing_to_param.invalid? && !missing_to_param.errors[:to].empty?, 'Param should be invalid without :to')
		
		invalid_date = FerryParam.create(@invalid_date_format_outbound)
		assert(invalid_date.invalid? && !invalid_date.errors[:outbound_date_string].empty?, 'Outbound date must be in format of 20131012')
		
		invalid_date = FerryParam.create(@invalid_date_format_inbound)
		assert(invalid_date.invalid? && !invalid_date.errors[:inbound_date_string].empty?, 'Inbound date must be in format of 20131014')

		# Correct case
		correct_param = FerryParam.create(@correct_param_1)
		assert(correct_param.valid?, "Shouldn't contain any errors")
		assert(correct_param.is_roundtrip, 'Should be roundtrip param')

		correct_param = FerryParam.create(@correct_param_2)
		assert(correct_param.valid?, "Shouldn't contain any errors")
		assert(!correct_param.is_roundtrip, 'Should be oneway param')
	
		correct_param = FerryParam.create(@correct_param_3)
		assert(correct_param.valid?, "Shouldn't contain any errors")
		assert(correct_param.outbound_date.strftime('%m%d%Y') == (Date.today + 3).strftime('%m%d%Y'), "Default date must be 3 days from today")
		assert(!correct_param.is_roundtrip, 'Should be oneway param')
	end

	class MockScraper < BaseScraper
		set_id 'mock'
	end

	class MockScraper2 < BaseScraper
		set_id 'mock_with_custom_error'
		
		def prepare_search
			add_error 'This is the error message'
		end
	end

	def test_base_scraper_static_methods
		assert(BaseScraper.contains?('mock'), 'Declared scraper must be in')

		assert(BaseScraper.get('mock').is_a?(MockScraper), 'Must receive correct instance from the factory')
	end

	def test_base_scraper_instance_methods
		m = BaseScraper.get('mock')

		assert(m.prepare_ferry_search(@missing_to_param).invalid? && !m.errors[:to].empty?, 'Param should be invalid without :to')
		assert(m.prepare_ferry_search(@missing_from_param).invalid? && !m.errors[:from].empty?, 'Param should be invalid without :from')
		assert(m.prepare_ferry_search(@invalid_date_format_inbound).invalid? && !m.errors[:inbound_date_string].empty?, 'Inbound date must be in format of 20131012')
		assert(m.prepare_ferry_search(@invalid_date_format_outbound).invalid? && !m.errors[:outbound_date_string].empty?, 'Outbound date must be in format of 20131012')

		assert(m.prepare_ferry_search(@correct_param_1).valid?, "Shouldn't contain any errors")
		assert(m.prepare_ferry_search(@correct_param_2).valid?, "Shouldn't contain any errors")
		assert(m.prepare_ferry_search(@correct_param_3).valid?, "Shouldn't contain any errors")

		m2 = BaseScraper.get('mock_with_custom_error')
		assert(m2.prepare_ferry_search(@correct_param_1).invalid?, "Should contain error even the data is correct")
	end

end